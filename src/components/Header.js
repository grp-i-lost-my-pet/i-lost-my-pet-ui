import React from 'react';
import {Avatar} from '@material-ui/core'
import {Dehaze, Search} from '@material-ui/icons'

function Header(){
    return(
        <div className="header-container">
            {/*Header left */}
            <div className="flex-center ml-8">
                <Dehaze />
            </div>

            {/*Header center */}
            <div className="flex-center opacity-100 bg-green-900 rounded-md border-2 border-gray-500">
                <Search />
                <input type="text" placeholder="Buscar" className="bg-transparent text-white	"/>
            </div>

            {/*Header right */}
            <div className="flex-center mr-8">
                <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
            </div>
            
        </div>
    );
}

export default Header;